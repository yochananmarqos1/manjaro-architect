##
##
## German Translation File
##
## Übersetzung: @eugen-b, @oberon
##
##

# Generic
_UseSpaceBar="De/Selektieren Sie die Einträge mit der [Leertaste]."
_AlreadyInst="Bereits installiert:"
_InstPkg="Installiere"
_All="Alle"
_Done="Fertig"
_PlsWaitBody="Bitte warten..."
_PassReEntBody="Passwort erneut eingeben."
_ErrTitle="Fehler"
_PassErrBody="Die eingegebenen Passwörter sind nicht gleich. Bitte erneut versuchen."

# Basics
_SelLang="Sprachauswahl"
_Lang="Sprache"
_Keep="Behalten"
_Change="Ändern"
_NoCon="Keine Internetverbindung."
_EstCon="Verbindung herstellen?"
_Config="Konfiguration"
_ApplySet="Spracheinstellungen werden\nangewendet ..."
_ChMenu="Menu wählen"
_ChMenuBody="Das Standardmenü wird Sie durch die Installation einer durch Manjaro vorkonfigurierten Desktopumgebung führen und zusätzliche Entscheidungen anbieten.\n\nDas erweiterte Menü kann zur Installation von Desktopumgebungen ohne Voreinstellungen und experimentellen Manjaroprofilen benutzt werden. Es beinhaltet auch eine Auswahl an Netzwerk- und Multimediaprogrammen sowie erweiterte System- und Sicherheitseinstellungen."

# Security and Tweaks
_SecMenuTitle="System- und Sicherheitseinstellungen"
_SecJournTitle="journald Logging berichtigen"
_SecCoreTitle="Coredump Logging deaktivieren"
_SecKernTitle="Zugriff auf Kernel-Logs einschränken"
_SecKernBody="Das Protokoll des Linux-Kernels könnte Informationen beinhalten, die zur Ausnutzung von Schwachstellen beitragen können und sensible Speicherbereiche aufzeigen.\n\nWenn in systemd-journald das protokollieren nicht abgeschaltet wurde, ist es möglich eine Datei in /etc/sysctl.d/ zu erstellen, welche Zugriff auf das Protokoll des Linux-Kernels nur mit Root-Rechten (z.B. mit sudo) erlaubt."
_Edit="Konfigurationsdatei bearbeiten"
_Disable="Deaktivieren"
_SecMenuBody="Einige nützliche und anfängerfreundliche System- und Sicherheitsoptimierungen.\n\nAnzeigen von Details durch Selektieren."
_SecJournBody="systemd-journald sammelt und speichert Kernel- und System-Protokolle, audit-Aufzeichnungen und Standard Output Fehlermeldungen von Services.\n\nStandardmäßig ist die persistente (nichtflüchtige) Journalgröße auf 10% der Rootpartitionsgröße limitiert: Bei einer Partitionsgröße von 500G ist der Datenspeicher für das Journal in /var/log somit auf max 50G beschränkt. 50M sollten ausreichend sein. Die Protokollierung kann vollständig deaktiviert werden, das Beheben von Systemfehlern wird dadurch jedoch gegf erschwert."
_SecCoreBody="Ein \"core dump\" ist eine Aufzeichnung von Arbeitsspeicherdaten während ein Prozess fehlschlägt.\n\nFür den Durchschnittsnutzer verbrauchen core dumps u.U. unnötig Systemresources und können auch sensible Daten wie Passwörter, Schlüssel u.ä. beinhalten\n\nStandardmäßig erzeugt systemd in /var/lib/systemd/coredump core dumps für alle Prozesse. Dieses Verhalten kann durch eine Konfigurationsdatei im Verzeichnis /etc/systemd/coredump.conf.d/ definiert werden."

# btrfs
_btrfsSVTitle="btrfs Subvolumes"
_btrfsSVBody="btrfs Subvolumes erzeugen?\n\nEin erstes Subvolume wird erzeugt und dann eingehängt. Weitere Subvolumes, die sich davon abzweigen, können danach erzeugt werden.\n\nÜberspringen Sie diese Schritte, um direkt zu den Einhägeoptionen zu gelangen."
_btrfsMSubBody1="Den Namen für das erste einzuhängende Subvolume eingeben (z.B. ROOT). Einhängeoptionen können dann gewählt werden. Nach dem Einhängen werden alle weiteren Subvolumes für"
_btrfsMSubBody2="sich davon abzweigen."
_btrfsSVErrBody="Leerzeichen oder keine Zeichen sind nicht erlaubt. Bitte nochmal eingeben."
_btrfsSVBody1="Gebe den Namen für das Subvolume ein"
_btrfsSVBody2="zu erzeugen in"
_btrfsSVBody3="Dieser Prozess wird wiederholt bis ein Sternchen (*) als Name für ein Subvolume eigegeben wird.\n\nErzeugte Subvolumes:"
_btrfsMntBody="[Leerzeichen] drücken, um die gewünschten Einhängeoptionen aus- oder abzuwählen.\nBitte achten Sie darauf, keine inkompatiblen Optionen zu wählen."
_btrfsMntConfBody="Bestätigen Sie die Auswahl der Einhängeoptionen:"

# zfs
_PrepZFS="ZFS (optional)"
_zfsNotSupported="Die Kernelmodule zur Unterstützung von ZFS konnten nicht gefunden werden"
_zfsAutoComplete="Die automatische ZFS-Bereitstellung wurde abgeschlossen"
_zfsMainMenuBody="ZFS ist ein flexibles und widerstandsfähiges Dateisystem, das Elemente der logischen Datenträgerverwaltung, RAID und traditioneller Dateisysteme kombiniert.  ZFS unter Linux erfordert eine spezielle Handhabung und ist nicht ideal für Anfänger.\n\nWählen Sie 'automatisch', um eine Partition auszuwählen und dem System zu erlauben, die Erstellung eines neuen zpools und von Datasets, die in '/', '/home' und '/var/cache/pacman' eingehängt sind, zu automatisieren.  Eine manuelle Konfiguration ist möglich, erfordert aber spezielle Kenntnisse über zfs."
_zfsMainMenuOptAutomatic="Automatisch konfigurieren"
_zfsMainMenuOptManual="Manuelle Konfiguration"
_zfsManualMenuTitle="Manuelles ZFS Setup"
_zfsManualMenuBody="Bitte wählen Sie unten eine Option"
_zfsManualMenuOptCreate="Erstelle einen neuen zpool"
_zfsManualMenuOptImport="Einen bestehenden zpool importieren"
_zfsManualMenuOptNewFile="Ein ZFS-Dateisystem erstellen und einhängen"
_zfsManualMenuOptNewLegacy="Ein Legacy-ZFS-Dateisystem erstellen"
_zfsManualMenuOptNewZvol="Erstelle ein neues ZVOL"
_zfsManualMenuOptSet="Eine Eigenschaft auf einem zfs-Dateisystem setzen"
_zfsManualMenuOptDestroy="Einen ZFS-Dataset löschen"
_zfsZpoolPartMenuTitle="Eine Partition auswählen"
_zfsZpoolPartMenuBody="Wählen Sie eine Partition aus, die den ZFS-Zpool aufnehmen soll"
_zfsZpoolCTitle="zpool-Erstellung"
_zfsZpoolCBody="Geben Sie den Namen für den neuen zpool ein"
_zfsZpoolCValidation1="zpool-Namen müssen mit einem Buchstaben beginnen und sind nur auf alphanumerische Zeichen und die Sonderzeichen : . - _ beschränkt"
_zfsZpoolCValidation2="zpool-Namen können nicht mit den reservierten Wörtern (log, mirror, raidz, raidz1, raidz2, raidz3, oder spare) beginnen"
_zfsZpoolImportMenuTitle="Importiere einen zpool"
_zfsZpoolImportMenuBody="Wählen Sie einen bestehenden Pool zum Importieren"
_zfsSelectZpoolMenuTitle="zpool Auswahl"
_zfsSelectZpoolMenuBody="Wählen Sie einen zpool aus der Liste"
_zfsMountMenuTitle="Einhängepunkt Auswahl"
_zfsMountMenuBody="Geben Sie einen Einhängepunkt für das Dateisystem ein"
_zfsMountMenuInUse="Dieser Einhängepunkt ist bereits in Verwendung, bitte wählen Sie einen anderen Einhängepunkt"
_zfsMountMenuNotValid="Das ist kein gültiger Einhängepunkt"
_zfsDSMenuNameTitle="ZFS Dataset"
_zfsDSMenuNameBody="Geben Sie einen Namen und einen relativen Pfad für das Dataset ein.  Wenn Sie beispielsweise möchten, dass das Dataset unter zpool/data/zname abgelegt wird, geben Sie 'data/zname' ein"
_zfsZvolSizeMenuTitle="ZVOL Größe"
_zfsZvolSizeMenuBody="Geben Sie die Größe des zvol in Megabytes(MB) ein"
_zfsZvolSizeMenuValidation="Sie müssen eine Zahl größer als 0 eingeben"
_zfsDestroyMenuTitle="Ein ZFS-Dataset löschen"
_zfsDestroyMenuBody="Wählen Sie das Dataset aus, das Sie dauerhaft löschen möchten.  Bitte beachten Sie, dass dies rekursiv alle untergeordneten Datasets mit Warnung löscht"
_zfsDestroyMenuConfirm1="Bitte bestätigen Sie, dass Sie alle Daten unwiderruflich löschen möchten"
_zfsDestroyMenuConfirm2="und die darin enthaltenen Daten aller seiner Kind-Elemente"
_zfsSetMenuTitle="Eine ZFS-Eigenschaft setzen"
_zfsSetMenuBody="Geben Sie die Eigenschaft und den Wert ein, den Sie einstellen möchten, indem Sie das Format property=mountpoint\n\n verwenden. Zum Beispiel könnten Sie eingeben:\ncompression=lz4\nor\nacltype=posixacl"
_zfsSetMenuSelect="Wählen Sie den Dataset aus, für den Sie eine Eigenschaft festlegen möchten"
_zfsSetMenuValidation="Eingabe muss das Format property=mountpoint haben"
_zfsCancelled="Vorgang abgebrochen"
_zfsFoundRoot="ZFS-Root auf '/' verwenden"
_zfsZpoolNoPool="Keine verfügbaren zfs-Pools gefunden"
_zfsDatasetNotFound="Keine Datasets verfügbar"

# Autopartition
_AutoPartBody1="ACHTUNG! SÄMTLICHE Daten auf"
_AutoPartBody2="werden gelöscht!\n\nEs wird eine 512MB Boot Partition erzeugt, gefolgt von einer zweiten Partition (root bzw '/') auf dem gesamten verbleibenden Speicherplatz."
_AutoPartBody3="Wenn Sie SWAP benutzen möchten, wählen Sie beim Einhängen die Option 'Swap File' aus.\n\nMöchten Sie fortfahren?"

# Error Messages. All others are generated by BASH.
_ErrNoMount="Mindestens eine Partition muss eingehängt sein."
_ErrNoBase="Die Manjaro Basis muss zuerst installiert werden."
_ErrNoKernel="Wenigstens ein Kernel muss ausgewählt sein."

# Vbox Installations
_VBoxInstTitle="VirtualBox Installation"
_VBoxInstBody="Falls aus irgendeinem Grund VirtualBox Gast-Module auf dem installierten System nicht geladen werden (z.B. niedrige Auflösung und Scrollbalken nach dem Bootvorgang), wird die einmalige Eigabe der folgenden Befehle das Problem beheben:\n\n$ su\n# depmod -a\n# modprobe -a vboxvideo vboxguest vboxsf\n# reboot"

# Select Config Files
_SeeConfOptTitle="Konfigurationsdateien überprüfen"
_SeeConfOptBody="Folgende Dateien können geprüft und geändert werden."
_SeeConfErrBody="Diese Datei existiert nicht."

# Pacman
_PrepPacKey="Pacman-Schlüssel auffrischen"

# LUKS / DM-Crypt / Verschlüsselung
_PrepLUKS="LUKS Verschlüsselung (optional)"
_LuksMenuBody="Mit dm_crypt verschlüsselte Block-Geräte und Volumes können ohne Passwort weder gesehen noch geöffnet werden."
_LuksMenuBody2="Eine separate /boot Partition ohne Verschlüsselung oder LVM wird benötigt (außer bei BIOS Grub)."
_LuksMenuBody3="Die automatische Option nutzt voreigenstelle Werte und wird für Anfänger empfohlen. Ansonsten besteht die Möglichkeit, das Chiffre und die Schlüssellänge manuell zu definieren."
_LuksOpen="Verschlüsselte Partition öffnen"
_LuksErr="Keine LUKS-verschlüsselte Partition gefunden."
_LuksOpenBody="Geben Sie einen Namen für das verschlüsselte Block-Gerät an. Es ist nicht notwendig, dem Namen '/dev/mapper/' voran zu stellen. Ein Beispiel ist angegeben."
_LuksEncrypt="Automatische LUKS Verschlüsselung"
_LuksEncryptAdv="Schlüssellänge und Chiffre definieren"
_LuksEncryptBody="Wählen Sie die Partition zum Verschlüsseln."
_LuksEncruptSucc="Fertig! Offen und bereit zum Einhängen per LVM (empfohlen) oder direkt."
_LuksPartErrBody="Mindestens zwei Partitionen sind für die Verschlüsselung erforderlich:\n\n1. Root (/) - von Typ Standard oder LVM-Partition.\n\n2. Boot (/boot or /boot/efi) - nur Standardpartitionstyp (außer bei bei BIOS Grub)."
_SelLuksRootBody="Wählen Sie die zu verschlüsselnde ROOT (/) Partition. Dorthin wird Manjaro installiert."
_LuksPassBody="Geben Sie ein Passwort zum Ver/Entschlüsseln der Partition ein. Dieses sollte nicht das gleiche sein wie für den Benutzer oder den Root sein."
_LuksWaitBody="Erzeuge verschlüsselte Root-Partition:"
_LuksWaitBody2="Block-Gerät or Volumen, das verwendet wird:"
_LuksCipherKey="Sobald die spezifizierten Flaggen geändert worden sind, werden sie automatisch mit dem Befehl 'cryptsetup -q luksFormat /dev/...' verwendet werden.\n\nHINWEIS: Schlüssel-Dateien werden nicht unterstützt; sie können manuell nach der Installation hinzugefügt werden. Spezifizieren Sie keine zusätzlichen Flaggen wie -v (--verbose) or -y (--verify-passphrase)."

# Logical Volume Management
_PrepLVM="Logical Volume Management"
_PrepLVM2="(optional)"
_LvmMenu="Logical Volume Management (LVM) erlaubt es, 'virtuelle' Festplatten (Volume Groups) und LV (Logical Volumes) aus physischen Festplatten und Partitionen zu erzeugen. Eine Volume Group muss zuerst erzeugt werden, dann ein oder mehrere Logical Volumes in ihr.\n\nLVM kann auch genutzt werden,um mehrere Logical Volumes (z.B. Root und Home) auf einer verschlüsselten Partition zu verwalten."
_LvmCreateVG="Erzeuge VG and LV(s)"
_LvmDelVG="Lösche Volume Groups"
_LvMDelAll="Lösche *ALLE* VGs, LVs, PVs"
_LvmDetBody="Vorhandenes Logical Volume Management (LVM) entdeckt. Activiere. Bitte warten ..."
_LvmPartErrBody="Es sind keine für LVM nutzbaren Partitionen vorhanden. Es ist mindestens eine nutzbare Partition erforderlich.\n\nFalls LVM bereits in Verwendung ist, können Sie es deaktivieren, um das zugrunde liegende physische Volumen für LVM wieder nutzen zu könenn."
_LvmNameVgBody="Wählen Sie einen Namen für die Volume Group (VG), die erzeugt werden soll.\n\nDie VG ist das neue 'virtuelle Speichergerät', welche aus den im nächsten Schritt zu wählenden Partitionen erzeugt wird."
_LvmNameVgErr="Ungültiger Name eigegeben. Der Name einer Volume Group darf alphanumerisch sein, aber keine Leerzeichen enhalten, nicht mit '/' anfangen, oder bereits vergeben sein."
_LvmPvSelBody="Wählen Sie die Partition(en), die als Physical Volume (PV) genutzt werden sollen."
_LvmPvConfBody1="Bestätigen Sie die Erstellung der Volume Group"
_LvmPvConfBody2="bestehend aus folgenden Partitionen:"
_LvmPvActBody1="Erzeuge und aktiviere Volume Group"
_LvmPvDoneBody1="Volume Group"
_LvmPvDoneBody2="wurde erzeugt."
_LvmLvNumBody1="Geben Sie die Anzahl von Logical Volumes (LVs) an für Volume Group"
_LvmLvNumBody2="Das letzte (oder das einzige) LV wird automatisch 100% des verbleibenden Speicherplatzes verwenden."
_LvmLvNameBody1="Geben Sie den Namen des zu erstellenden LV ein.\n\nDies ist der Namenvergabe für eine Partition ähnlich."
_LvmLvNameBody2="HINWEIS: Dieses LV wird automatisch den gesamten verfügbaren Speicherplatz auf der Volume Group einnehmen."
_LvmLvNameErrBody="Der eingegebene Name wird bereits benützt oder ist nicht erlaubt.\nVerwenden Sie nur alpha-numerische Zeichen, kein Leerzeichen oder '/'!"
_LvmLvSizeBody1="verbleibend"
_LvmLvSizeBody2="Geben Sie die Größe des LV in Megabytes (M) oder Gigabytes (G) ein.\nZ.B. wird bei 100M ein LV von 100 Megabytes erzeugt, bei 10G ein LV von 10 Gigabytes."
_LvmLvSizeErrBody="Ungültiger Wert eingegeben. Ein numerischer Wert mit einem 'M' (Megabytes) oder 'G' (Gigabytes) am Ende muss eingegeben werden.\n\nBeispiele: 100M, 10G, oder 250M. Der Wert darf nicht größer oder gleich der verbleibenden Größe der VG sein."
_LvmCompBody="Fertig! Alle Logical Volumes für die Volume Group wurden erzeugt.\n\nMöchte Sie sich das neue LVM-Schema anzeigen lassen?"
_LvmDelQ="Bestätigen Sie, dass Volume Group(s) bzw. Logical Volume(s) gelöscht werden sollen.\n\n Wenn eine Volume Group gelöscht wird, werden alle darin enthaltenen Logical Volumes ebenfalls gelöscht."
_LvmSelVGBody="Wählen Sie die zu löschende Volume Group. Alle darin enthaltenen Logical Volumes werden ebenfalls gelöscht."
_LvmVGErr="Keine Volume Groups gefunden."

# Show devices and partitions
_DevShowOpt="Blockgeräte auflisten (optional)"

# Check Requirements
_ChkTitle="Prüfe Voraussetzungen"
_ChkBody="Es wird geprüft, ob der Installer als Root gestartet wurde und es eine aktive Internetverbindung gibt.\nBitte warten..."
_RtFailBody="Der Installer muss als Root gestatet werden. Verlasse das Programm..."
_ConFailBody="Test der Internetverbindung schlug fehl. Verlasse das Programm..."
_ReqMetTitle="Voraussetzungen sind erfüllt."
_ReqMetBody="Alle Checks erfolgreich!"
_UpdDb="aktualisiere Datenbank ..."


# Installer Mirrorlist
_MirrorlistTitle="Mirrorliste"
_MirrorBranch="Wähle Manjaro Branche die benutzt werden soll"
_MirrorlistBody="Die Mirrorliste enthält die Adressen der Server, von denen Pacman die Pakete herunterlädt und installiert. Um die schnellsten Server zu finden, generieren Sie ZUERST eine Mirrorliste für ein Land, BEVOR Sie sie nach Geschwindigkeit ordnen. Sonst wird der Prozess sehr lange dauern.\nDie Datei pacman.conf kann editiert werden, um multilib und andere, externe Repositorien zu aktivieren.\n\nHinweis: Schließen Sie Textdateien mit '[Strg] + [x]' und bestätigen Sie das Speichern der Änderungen mit [j] oder verwerfen sie mit [n]."
_RankMirrors="Wählen Sie die gewünschten Mirror mit [Leertaste] oder [Enter] aus und bestätigen Sie mit [OK] unten auf der Seite."
_MirrorbyCountry="Generiere Mirrorlist für ein Land"
_MirrorEdit="Manuelle Änderung der Mirrorlist"
_MirrorRankTitle="Ordne Mirror nach Geschwindigkeit (all)"
_MirrorRankTitleContinent="Ordne Mirror nach Geschwindigkeit (continent)"
_MirrorRestTitle="Ursprüngliche Mirrorlist wiederherstellen"
_MirrorRankBody="Finden der schnellsten Server aus der Mirrorlist"
_MirrorNoneBody="Eine Kopie der ursprünglichen Mirrorlist konnte nicht gefunden werden."
_MirrorCntryBody="Eine Liste der Mirrors nach Land wird erzeugt."
_MirrorGenQ="Die generierte Mirrorlist für den Installer nutzen?"
_MirrorConfig="Editiere Pacman-Mirror-Konfiguration"
_MirrorPacman="Editiere Pacman-Konfiguration"
_MIrrorPacQ="Die editierte Pacman-Konfiguration für das installierte System nutzen? Wenn ja, wird die Datei auf das installierte System nach der Installation von Base-Paketen kopiert."

# Set Keymap (vconsole)
_VCKeymapTitle="Konsolensprache einstellen"
_DefKeymap="Die automatisch ausgewählte Tastaturbelegung ist"
_VCKeymapBody="Eine virtuelle Konsole ist das Kommandozeilen-Interface einer nicht-graphischen Umgebung.\nIhr Tastaturlayout ist unabhängig von der Desktop-Umgebung."

# Set Xkbmap (environment)
_XkbmapBody="Wählen Sie das Tastaturlayout für die Desktop-Umgebung."

# Set Locale
_localeBody="Die Locale bestimmt die angezeigte Sprache, Zeit-, Datum-, Zahlen-Format etc.\n\nDas Format ist nach dem Schema sprache_LAND (z.B. de_DE ist Deutsch, Deutschland; de_AT ist Deutsch, Österreich)."
_langBody="Wählen Sie die Systemsprache. Zur einfacheren Fehlerbehebung wird Englisch empfohlen. Das Format ist language_COUNTRY (z. B. en_US ist englisch, Vereinigte Staaten; en_GB ist englisch, Großbritannien)."

# Set Timezone
_TimeZBody="Die Zeitzone wird genutzt, um die System-Uhr korrekt einzustellen."
_TimeSubZBody="Wählen Sie die Stadt, die am nächsten zu Ihnen liegt.."
_TimeZQ="Setze die Zeitzone als"

# Set Hardware Clock
_HwCBody="UTC ist der universelle Zeit-Standard und wird empfohlen, es sei denn man installiert im Dual-Boot mit Windows."

# Generate FSTAB
_FstabBody="Die Datei FSTAB (File System TABle) definiert, welche Speicher-Blockgeräte und Partitionen eingehängt und wie sie genutzt werden.\n\nDie Option mit UUID (Universally Unique IDentifier) wird empfohlen.\n\nFalls keine Label für die Partitionen vergeben wurden, werden Geräte-Bezeichner als Label verwendet."
_FstabErr="Die Option \"Part UUID\" wird nur für UEFI/GPT-Installationen verwendet."
_FstabDevName="Name des Blockgeräts"
_FstabDevLabel="Label des Blockgeräts"
_FstabDevUUID="UUID des Blockgeräts"
_FstabDevPtUUID="UEFI Part UUID"

# Set Hostname
_HostNameBody="Der Host-Name wird zur Identifikation des Rechners in einem Netzwerk genutzt.\n\nEr darf nur alphanumerische Zeichen enthalten, sowie Bindestriche (-), diese jedoch nicht am Anfang oder am Ende und darf nicht länger als 63 Zeichen lang sein."

# Set Root Password
_PassRtBody="Root-Passwort eingeben"
_PassRtBody2="Root-Passwort wiederholen"

# Create New User
_NUsrTitle="Neuen Benutzer anlegen"
_NUsrBody="Benutzernamen eingeben. Nur Kleinbuchstaben verwenden."

# Username Error
_NUsrErrTitle="Fehler im Benutzernamen"
_NUsrErrBody="Ein inkorrekter Benutzername wurde eingegeben. Bitte erneut versuchen."

# Set User
_PassNUsrBody="Passwort eingeben für"
_NUsrSetBody="Lege Benutzer an und setze Gruppen..."
_DefShell="Wähle Standard."

# Mounting (Partitions)
_MntStatusTitle="Einhänge-Status"
_MntStatusSucc="Einhängen erfolgreich!"
_MntStatusFail="Einhängen gescheitert!"
_WarnMount1="WICHTIG: Partitionen können eingehängt werden, ohne dass sie formatiert werden müssen.\nWählen Sie dazu die Option"
_WarnMount2="zu Beginn des Dateisystem-Menüs aus.\n\nStellen Sie sicher, dass die Einhänge- und Formatierungsoptionen korrekt sind, da es keine weiteren Warnungen geben wird, außer bei ungültiger UEFI-Boot-Partition."

# Select Device (installation)
_DevSelTitle="Wähle Blockgerät"
_DevSelBody="Blockgeräte (/dev/) sind die für die Installation zur Vefügung stehnenden Festplatten und USB-Sticks. Das erste ist /sda, das zweite /sdb usw...\n\nFalls Sie von einem USB-Stick booten, auf dem Manjaro-Architect installtiert ist, seien Sie vorsichtig, da dieser USB-Stick auch als verfügbar aufgelistet sein wird!"

# Partitioning Tool
_PartToolTitle="Partitionierungs-Tool"
_PartToolBody="Eine automatische, anfängerfreundliche Partitionierungs-Option steht zur Verfügung. Anstonsten wird für Systeme mit BIOS cfdisk empfohlen, für UEFI - parted.\n\nWählen  Sie kein nur-UEFI/GPT Partitionierungs-Tool für ein BIOS/MBR-System, da dies ersthafte Probleme zur Folge haben wird, einschließlich eine  nicht-bootbare Installation."
_PartOptAuto="Automatische Partitionierung"
_PartOptWipe="Sicheres Löschen der Daten auf dem Blockgerät (optional)"
_AutoPartWipeBody1="WARNUNG: ALLE Daten auf"
_AutoPartWipeBody2="werden mithilfe des Befehls 'wipe -Ifre' gelöscht. Dieser Vorgang kann abhängig von der Größe desBlockgeräts sehrlange dauern.\n\nMöchten Sie fortfahren?"

# Partitioning Error
_PartErrBody="BIOS-Systeme brauchen mindestens eine Partition (ROOT).\n\nUEFI-Systeme brauchen mindestens zwei Partitiones (ROOT und UEFI)."

# File System
_FSTitle="Dateisystem wählen"
_FSBody="Ext4 wird empfohlen. Nicht alle Dateisysteme sind für Root- oder Boot-Partitionen einsetzbar. Alle haben verschiedene Features aber auch Beschränkungen."
_FSSkip="Nicht formatieren"
_FSMount="Einhängen"
_FSWarn1="Daten auf"
_FSWarn2="werden gelöscht"

# Select Root
_SelRootBody="ROOT-Partition wählen.\nHierhin wird Manjaro installiert."

# Select SWAP
_SelSwpBody="SWAP-Partition wählen.\nWenn sie ein Swapfile nutzen wollen, wird dieses in der Größe ihrer RAM erzeugt."
_SelSwpNone="Kein"
_SelSwpFile="Swapfile"

# Select UEFI
_SelUefiBody="UEFI-Partition wählen. Das ist eine spezielle Partition zum hochfahren (booten) von UEFI-Systemen."

# Format UEFI
_FormUefiBody="Die UEFI-Partition"
_FormUefiBody2="ist bereits formatiert.\n\nUmformatieren? Das wird ALLE Daten, die auf dieser sich bereits befinden, löschen."

# UEFI Mountpoint
_MntUefiBody="UEFI-Einhängepunkt auswählen.\n\n
/boot/efi wird für Dualboot-Systeme empfohlen.\n
/boot wird für systemd-boot benötigt."
_MntUefiCrypt="UEFI-Einhängepunkt auswählen.\n\n
/boot/efi wird für Dualboot-Systeme empfohlen und ist für die Verschlüsselung der gesamten Festplatte erforderlich. Verschlüsseltes /boot wird nur von grub unterstützt und kann zu einem langsamen Start führen.\n\n
/boot wird für systemd-boot und für refind bei Verwendung von Verschlüsselung benötigt."

# Extra Partitions
_ExtPartBody="Wählen Sie zusätliche einzuhängende Partitionen in beliebiger Reihenfolge oder 'Fertig' zum Verlassen."

# Extra Partitions
_ExtPartBody1="Bestimmen Sie den Einhängepunkt für die Partition. Stellen Sie sicher, dass der Name mit einem Schrägstrich ('/') beginnt. Beispieie:"

# Extra Partition Naming Error
_ExtErrBody="Partition kann nicht eingehängt werden wegen eines Problems mit dem Namen des Einhängepunktes. Der Name muss mit einem Schrägstrich ('/') beginnen."

# Install Base
_WarnInstBase="Auf dieser Partition wurde anscheinend bereits eine Manjaro Basis installiert.\nDennoch fortfahren?"
_InstBseTitle="Installiere Basissystem"
_InstFail="Installation gescheitert."
_InstBseBody="Standard: Empfohlen für Anfänger. Wählen von bis zu zwei Kerneln (linux und linux-lts) und optional die Paketgruppe base-devel. Die Pakete sudo, btrfs-progs,f2fs-tools werdenebenfalls installiert.\n\nFortgeschritten: Wählen von bis zu vier Kerneln (linux, linux-lts, grsec, zen) und Kontrolle über die Wahl einzelner Pakete aus den Gruppen base und base-devel. Zusätzliche Konfiguration für grsec und zen kann für Virtualbox und NVIDIA erfoderlich sein.\n\nHINWEIS: Ess muss mindestensein Kernel gewählt werden, falls noch kein Kernel installiert wurde."
_InstStandBseBody="Die Paketgruppe base wird automatisch installiert. Die Gruppe base-devel wird benötigt, wenn man die Arch User Repository (AUR) nutzen will."
_InstStandBase="Standard-Installation"
_InstAdvBase="Fortgeschrittene Installation"
_InstAdvBseBody="WARNUNG: Dies ist für erfahrene Benutzer. Neuen Usern sei die Variante 'Standard-Installation' empfohlen."
_InstAdvWait="Sammele Paketbeschreibungen."
_InstGrub="Installiere GRUB"

# Install BIOS Bootloader
_InstBiosBtTitle="Installiere BIOS Bootloader"
_InstGrubBody="Das Blockgerät für die Installation von GRUB kann im nächsten Schritt ausgewählt werden\n\Os-prober wird zur automatischen Erkennung von bereits installierten Systemen auf anderen Partitionen benötigt."
_InstBiosBtBody="Grub2 wird für Anfähnger empfohlen. Das Blockgerät für den Bootloader kann festgelegt werden.\n\nSyslinux ist eine leichtere und simplere Alternative, die jedoch nur mit ext4/btrfs Dateisystemen funktioniert."
_InstSysTitle="Installiere Syslinux"
_InstSysBody="Syslinux in den Master Boot Record (MBR) oder in Root (/) installieren?"

# Install UEFI Bootloader
_InstUefiBtTitle="Installiere UEFI Bootloader"
_InstUefiBtBody="Installiere UEFI Bootloader GRUB."
_SetBootDefBody="Für manche UEFI-Firmware ist es nötig, den EFI Stub als 'bootx64.efi' nach"
_SetBootDefBody2="zu kopieren, damit der Bootloader als default erkannt wird.\n\nDieses Vorgehen wird empfohlen, außer es ist bereits ein default Bootloader deklariert, oder man beabsichtigt, mehrere Bootloader zu installieren.\n\nSetze den Bootloader als default?"

# efi file set for Grub
_SetDefDoneBody="wurde als default Bootloader gesetzt."

# Graphics Card Menu
_GCtitle="Grafikkarten-Menu"
_GCBody="Wählen Sie Nouveau für ältere NVIDIA-Grafikkarten. Falls Ihre Grafikkarte nicht aufgelistet ist, wählen Sie 'Unbekannt / Generisch'."
_GCUnknOpt="Unbekannt / Generisch"

# NVIDIA Configruation Check
_NvidiaConfTitle="NVIDIA Konfigurations-Check"
_NvidiaConfBody="Eine Basis-Konfigurationsdatei für NVIDIA wurde erstellt. Bitte, überprüfen Sie diese, bevor Sie fortfaren."

# Graphics Card Detection
_GCDetTitle="Gefunden"
_GCDetBody="ist Ihre Grafikkarte oder Virtualisierungs-Software"
_GCDetBody2="- Wählen Sie 'Ja', um den OPEN-SOURCE Treiber zu installieren.\n\n- Wählen Sie 'Nein' zum Öffnen des Grafikkarten-Menus, welches den proprietären NVIDIA Treiber enthält."

#  Install DE Info
_DEInfoBody="Mehrere Umgebungen können installiert werden.\n\nGnome und LXDE kommen mit einem Display Manager.\n\nCinnamon, Gnome und KDE kommen mit einem Network Manager."

# Install DE Menu
_InstDETitle="Installiere Desktop Environments"
_InstManDEBody="Wähle eine Manjaro Umgebung."
_ErrInit="Falsches Initsystem"
_WarnInit="ist nur für Systemd vefügbar\nAuswahl anpassen:"
_DiffPro="Anderes Profil wählen"
_InstSystd="Installiere Systemd Basis"
_InstDEBody="Desktop Environments und ihre zugehörigen Paketgruppen werden als erste aufgelistet."
_ExtraTitle="Voll oder Minimal?"
_ExtraBody="Diese Edition wird in zwei Ausführungen angeboten"

# Install Common Packages
_InstComTitle="Installiere gänge Pakete"
_InstComBody="Einige Umgebungen benötigen zusätzliche Pakete, um besser zu funktionieren."

# Display Manager
_DmChTitle="Installiere Display Manager"
_DmChBody="GDM hat Gnome-Shell als Abhängigkeit. sddm ist für Plasma empfohlen. LightDM wird lightdm-gtk-greeter enthalten. SLiM wird nicht mehr entwickelt."
_DmDoneBody="Display Manager wurde installiert und aktiviert."

# Network Manager
_InstNMTitle="Installiere Network Manager"
_InstNMBody="Network Manager ist empfohlen, besonders für WLAN- und PPPoE/DSL-Verbindungen"
_InstNMErrBody="Network Connection Manager wurde installiert und eingesetzt."

# Welcome
_WelTitle="Willkommen zum"
_WelBody="Dieser Installer wird die neuesten Pakete aus den Manjaro-Repositorien für Sie herunterladen und installieren.\n\nMENU-NAVIGATION:\n- Auswahl durch Drücken der Ziffer der Menu-Option oder durch Navigieren mit den [Hoch]/[Runter]-Tasten\n- Bestätigen mit der [Eingabetaste]\n- Wechseln zwischen den Knöpfen mit [Tab] oder [Links]/[Rechts]-Tasten\n- Lange Listen können mit [BildHoch]/[BildRunter] schnell durchlaufen werden,\n- Eingabe eines Buchstaben springt auf die Position des ersten entsprechenden Listenelements.\n\nKONFIGURATIONS- & PAKET-OPTIONEN:\nIn den Checklisten sind einzelne Standardpakete (default) vorausgewählt.\nBenutzen Sie die [Leertaste] zum Abwählen und Auswählen weiterer."

# Preparation Menu
_PrepMenuTitle="Installation vorbereiten"
_PrepMenuBody="Die gewählte Konsolensprache gilt für den Installer sowie für das installierte System."
_PrepKBLayout="Tastaturlayout festlegen"
_PrepMirror="Installer-Mirrorliste konfigurieren"
_PrepPartDisk="Festplatte partitionieren"
_PrepMntPart="Partitionen einhängen"
_Back="Zurück"

# Install Base Menu
_PkgList="Verarbeite Paketliste"
_InstBsMenuTitle="Basis installieren"
_InstBseMenuBody="Die zu installierenden Pakete müssen von einem Mirror heruntergeladen werden.\nDie voreingestellte Branche ist 'stable'.\nUm die Downloadgeschwindigkeit zu optimieren oder um die Branche zu wechseln, benutzen Sie die Option [$_PrepMirror]."
_InstBse="Basispakete installieren"
_ChsInit="Wähle das Initsystem"
_Note="Achtung!"
_WarnOrc="Folgende Manjaro Profile sind nicht mit openrc kompatibel:"
_ChsAddPkgs="Wähle zusätzliche Kernel-Module"
_InstBootldr="Bootloader installieren"
_InstDrvTitle="Installiere Gerätetreiber"
_InstDrvBody="Manche Netzwerk- und Grafikkarten\nbenötigen spezielle Treiber.\nOptionen 1 und 2 wählen Treiber automatisch aus,\nmit Optionen 3 und 4 können spezifische Treiber ausgewählt werden."
_InstFree="Auto-Installation freie Treiber"
_InstProp="Auto-Installation proprietäre Treiber"
_SelDDrv="Grafiktreiber auswählen"
_InstAllDrv="Freie Treiber installieren"

# Configure Base Menu
_ConfBseMenuTitle="Basis konfigurieren"
_ConfBseBody="Grundkonfiguration der Basis."
_ConfBseFstab="FSTAB generieren"
_ConfBseHost="Hostname setzen"
_ConfBseTimeHC="Zeitzone und Uhr einstellen"
_ConfBseSysLoc="System Locale einstellen"
_ConfBseSysLang="Systemsprache einstellen"
_RunMkinit="Mkinitcpio ausführen"
_RunUpGrub="GRUB aktualisieren"

# User Menu
_ConfUsrRoot="Root-Passwort setzen"
_ConfUsrNew="Neue(n) Benutzer hinzufügen"

# Graphics Menu
_InstGrMenuTitle="Grafische Oberfläche installieren"
_InstGrMenuBody="Vor der Installation einer Desktop-Umgebung MÜSSEN zuerst die Grafik-, Eingabe- und Soundtreiber installiert werden. Dazu gehört auch die Installation von Grafikkartentreibern."
_InstDEMenuTitle="Wähle eine vollständige Manjaro Edition oder ein unkonfigurierte Desktop-Umgebung"
_InstGrMenuDS="Installiere Anzeige Server"
_InstGrMenuDSBody="Zusätzlich zu den xorg- und wayland-Optionen werden auch Treiber für Eingabegeräte (xf86-input-) aufgeführt."
_InstGrMenuDD="Installaliere Grafiktreiber"
_InstGrDrv="Grafiktreiber zur Installation auswählen"
_WarnInstGr="Kein Grafiktreiber ausgewählt."
_InstDEStable="Manjaro Desktop installieren"
_InstDEGit="Install Manjaro Desktop (development profiles)"
_InstGrDE="Installiere Desktop-Umgebung"
_InstPBody="Dies installiert eine Liste von Anwendungs- und Konfigurations-Paketen, entsprechend der gleichnamigen Manjaro Edition."
_InstDE="Unkonfigurierte Desktop-Umgebung installieren"
_InstGrMenuGE="Installiere Grafische Umgebung"
_InstGrMenuDM="Installiere Display Manager"

# Networking Menu
_InstNMMenuTitle="Installiere Netzwerk Komponenten"
_InstNWDrv="Netzwerktreiber"
_SelNWDrv="Netzwerktreiber auswählen"
_InfoNWKernel="Unterstützung für die Netzwerkkarte ist im Kernel eingebaut\nKeine Installation notwendig."
_InstNMMenuBody="Für Netzwerk- und Wireless-Geräte sind möglicherweise zusätzliche Pakete erforderlich. Einige drahtlose Geräte benötigen möglicherweise auch zusätzliche Firmware, um zu funktionieren."
_InstNMMenuPkg="Installiere Wireless Treiber Pakete"
_InstNMMenuNM="Install Network Connection Manager" # translate me !
_InstNMMenuCups="CUPS / Drucker-Pakete installieren"
_InstNMMenuPkgBody="Die wichtigsten WLAN-Pakete werden vorab geprüft, wenn ein drahtloses Gerät erkannt wurde. Wenn Sie sich bei zusätzlicher Firmware unsicher sind, können alle Pakete installiert werden."
_SeeWirelessDev="Drahtloses Gerät anzeigen (optional)"
_WirelessShowTitle="Drahtloses Gerät"
_WirelessErrBody="Kein Drahtloses Gerät."
_InstCupsBody="CUPS (Common Unix Printing System) ist das standardbasierte, quelloffene Drucksystem, das von Apple Inc. für OS X und andere UNIX-ähnliche Betriebssysteme entwickelt wurde. Samba ermöglicht die Datei- und Druckerfreigabe zwischen Linux- und Windows-Systemen."
_InstCupsQ="org.cups.cupsd.service auf dem dem installieren System aktivieren?"

# Install Multimedia Support Menu
_InstMultMenuTitle="Multimedia-Unterstützung installieren"
_InstMultMenuBody="Barrierefreie Pakete helfen Menschen mit Seh- und/oder Hörbehinderungen. Die Option 'Benutzerdefinierte Pakete' ermöglicht die Installation von benutzerdefinierten Paketen."
_InstMulSnd="Installiere Soundkartentreiber"
_InstMulSndBody="ALSA bietet kernelgesteuerte Soundkartentreiber. PulseAudio dient als Proxy für ALSA."
_InstMulCodec="Installiere Multimeia Codecs"
_InstMulAcc="Installiere Bedienugshilfen Pakete"
_InstMulAccBody="Wählen Sie die gewünschten Bedienungshilfen Pakete."
_InstMulCust="Benutzerdefinierte Pakete installieren"

# Codecs Menu
_InstMulCodBody="GStreamer ist ein Pipeline-basiertes Multimedia-Framework. Die ersten beiden Optionen sind die aktuelle und die Legacy (gstreamer0.10) Paketgruppe. Xine ist ebenfalls aufgeführt."

# Custom Packages Box
_InstMulCustBody="Geben Sie die (exakten) Namen von Paketen aus den Manjaro Repositorien - getrennt mit Leerzeichen - ein.\n\nZum Beistiel, um Firefox, VLC, und HTop zu installieren: firefox vlc htop"

# Main Menu
_MMTitle="Hauptmenü"
_MMBody="Die Menüeinträge sollten der Reihe nach abgearbeitet werden bis die Installation mit 'Fertig' abgeschlossen werden kann."

# Final Check
_BaseCheck="Basis wurde nicht installiert"
_BootlCheck="Kein Bootloader installiert"
_FstabCheck="Fstab wurde nicht erzeugt"
_GCCheck="Kein Grafiktreiber installiert"
_LocaleCheck="Locales wurden nicht erzeugt"
_RootCheck="Kein Root-Passwort gesetzt"
_UserCheck="Kein Benutzer eingerichtet"

# Chroot
_ChrootReturn="\nHier können Sie in Ihr installiertes System chrooten und Änderungen vornehmen, fast so als würden Sie in die Installation booten.\n\nTippen Sie \"exit\" zum Verlassen oder \"fg\" um zum Installer zurückzukehren.\n "
_EnterChroot="In die Installation chrooten"
_ChrootTitle="Chroot in Bestehende Installation"

#Refind
_InstRefindTitle="'refind' installieren"
_InstRefindBody="Dies installiert refind und konfiguriert es, automiatisch Ihren Kernel zu erkennen. Keine Unterstützung für verschlüsselte /boot Partition und intel microcode. Diese erfordern manuelle Boot-Einträge oder einen anderen Bootloader."
_RefindReady="Refind erfolgreich installiert"
_bootloaderInfo="Refind kann unabhängig oder als grafisches Bootmenu in Verbindung mit anderen Bootloaders verwendet werden. Es erkennt während des Bootvorgangs bootfähige Systeme automatisch.\nGRUB unterstützt verschlüsselte /boot Partitionen und erkennt bootfähige Systeme wenn Ihre Kernel aktualisiert werden, kann .iso-Dateien von Speichermedien booten und stellt automatische Boot-Einträge fpr btrfs-Schnapschüsse zur Verfügung.\nSystemd-boot ist sehr leicht und einfach mit wenig Automation. Es erkennt Windows automatisch aber ist ansonsten für Multiboot ungeeignet."

# Systemd-boot
_InstSystdBBody="Dies installiert Systemd-boot und generiert Boot-Einträge für aktuell installierte Kernel. Die Kernel müssen für diesen Bootloader auf der UEFI Partition installiert und diese als /boot eingehängt sein."
_SystdBReady="Systemd-boot wurde installiert"

#Meta menu
_InstCrMenuTitle="CLI System installieren"
_InstCrMenuBody="Dies installiert ein Manjaro-Basissystem ohne grafische Oberfläche. Schritte 1-3 sind für ein funktionierendes System erforderlich, der Rest optional."
_InstDsMenuTitle="Desktop-System installieren"
_InstDsMenuBody="Dies installiert eine vollständige Manjaro Edition mit grafischer Oberfläche. Schritte 1-3
Dies installiert die vollständige Manjaro-Edition mit grafischer Desktop-Umgebung. sind für ein funktionierendes System erforderlich, der Rest optional."
_InstCsMenuTitle="Individuelles System installieren"
_InstCsMenuBody="Dies installiert ein Manjaro-Basissystem ohne grafische Oberfläche. Schritte 1-4 sind für ein funktionierendes System erforderlich, der Rest optional."
_MMNewBody="Wählen Sie die Installationsart nach dem Einhängen der Partitionen. Wenn Sie unsicher sind, wählen Sie 'Desktop-System' für eine Standard-Installation."

#System rescue
_SysRescTitle="System reparieren"
_SysRescBody="Dieses Menu enthält Werkzeuge zur Reparatur eines beschädigten Systems."
_RmPkgs="Pakete entfernen"
_RmPkgsMsg="Pakete nach Namen suchen.
TAB zur Auswahl weiterer Pakete."
_RmPkgsPrmpt="Zu entfernendes Paket"
_AddPkgsPrmpt="Paket"
_AddPkgs="Wähle zusätzliche Pakete zur Installation.
Suchen durch Eingabe des Namens.
TAB zur Auswahl weiterer Pakete
Fortfahren/Abbrechen mit ENTER."

#Extra
_ExtraPkgTitle="Zusätzliche Pakete"
_ExtraPkgBody="Möchten Sie der Installation zusätzliche Pakete hinzufügen?"

_TweaksMenuTitle="System-Optimierungen"
_PerfBody="Einstellungen, um Ihr System für eine bestimmte Arbeitslast zu konfigurieren"
_TweaksBody="Verschiedene Konfigurationsoptionen"
_PerfMenu="Leistung"
_SetSchd="I/O schedulers"
_SetSwap="Swap-Konfiguration"
_AutologEnable="Automatische Anmeldung aktivieren"
_HibrnEnable="Ruhezustand einrichten/aktivieren"
_LogMenu="Systemprotokolle anzeigen"
_LogBody="Parse system logs with fzf" # translate me !
_DataRecMenu="Datenwiederherstellung"
_DataRecBody="Verschiedene Tools zur Datensicherung und wiederherstellung"

_HostCache="pacman cache Auswählen"
_HostCacheBody="Möchten Sie den pacman-Cache des laufenden Systems anstelle des Installationsziels verwenden? Dies kann die Größe der benötigten Downloads bei der Installation reduzieren."

#RAID menu
_PrepRAID="RAID (optional)"

_RAIDLevelTitle="Wählen Sie ein RAID level."
_RAIDLevel0="Disk striping"
_RAIDLevel1="Spiegelung"
_RAIDLevel5="verteilte Parität, (1 Laufwerkstoleranz, erfordert 3 Festplatten)"
_RAIDLevel6="doppelte Parität, (2 Laufwerkstoleranz, erfordert 4 Festplatten)"
_RAIDLevel10="raid 1+0, (benötigt 4 Festplatten)"

_PartitionSelectTitle="Partion wählen"
_PartitionSelectDescription="Wählen Sie die Partitionen aus, die Sie für diesen RAID-Verbund verwenden möchten."

_DeviceNameTitle="Gerätename"
_DeviceNameDescription="Wie möchten Sie das RAID-Gerät benennen? \nBeispiel: Standardmäßig wird das erste Raid-Gerät im System md0 genannt."
_DeviceNamePrefixWarning="(don't prefix with /dev/md/)"  # translate me !

_ArrayCreatedTitle="Array erstellt"
_ArrayCreatedDescription="Das RAID-Array wurde erfolgreich erstellt."
